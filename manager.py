from unittest import TestLoader, runner
from unittest.case import TestCase
import click

from app import app


@click.group()
def c():
  ...


@c.command()
def runserver():
  app.run(debug=True)


@c.command()
def tests():
  loader = TestLoader()
  tests = loader.discover("tests/")
  testrunner = runner.TextTestRunner()
  testrunner.run(tests)


c()
