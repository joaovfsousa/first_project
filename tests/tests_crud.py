from unittest import TestCase
from app import app
from flask import url_for


class FirstTestCase(TestCase):
  def setUp(self):
    self.app = app
    self.app_context = self.app.test_request_context()
    self.app_context.push()
    self.client = self.app.test_client()
    self.app.testing = True

  def test_root_should_return_200(self):
    result = self.client.get(url_for("crud.root"))

    self.assertEqual(200, result.status_code)

  def test_get_users(self):
    esperado = [{"nome": "Eduardo", "sobrenome": "Mendes"}]
    result = self.client.get(url_for("crud.get_users"))

    self.assertEqual(esperado, result.json)

  def test_post_users(self):
    from json import dumps
    request_ob = {"nome": "Eduardo", "sobrenome": "Mendes"}
    esperado = "OK"
    result = self.client.post(url_for("crud.post_user"),
                              data=dumps(request_ob))
    self.assertEqual(esperado, result.data.decode("ascii"))
