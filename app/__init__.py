from flask import Flask
from .crud import crud_bp

app = Flask(__name__)
app.register_blueprint(crud_bp, url_prefix="/")
