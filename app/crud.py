from flask import Blueprint, jsonify, request


crud_bp = Blueprint("crud", __name__)


@crud_bp.route('/')
def root():
    return jsonify({})


@crud_bp.route('/get-users')
def get_users():
  users = [{"nome": "Eduardo", "sobrenome": "Mendes"}]
  return jsonify(users)


@crud_bp.route('/post-user', methods=["POST"])
def post_user():
  user = request.get_json(force=True)
  print(user)
  return "OK"
